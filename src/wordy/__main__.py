from datetime import date
from enum import Enum, auto, unique
import importlib.resources as resources
import json
import os
from pathlib import Path
from random import choice
import readline
import sys

from . import data


MAX_GUESSES = 6

with resources.path(data, "answers_by_date.json") as path:
    ANSWERS_FILE = path

with resources.path(data, "valid.txt") as path:
    VALID_FILE = path


def main():
    use_argv = len(sys.argv) > 1

    while True:
        if use_argv:
            tokens = sys.argv[1:]
            use_argv = False
        else:
            command = input("> ").lower()
            tokens = command.split(" ")

        if tokens[0] == "h" or tokens[0] == "?" or tokens[0] == "help":
            print("Available commands:")
            print("  p: Play today's wordy.")
            print("  p YYYY-MM-DD: Play wordy for the given date.")
            print("  s: Show the stats.")
            print("  h: Show this help message.")
            print("  q: Quit.")

        elif tokens[0] == "p" or tokens[0] == "play":
            try:
                target_date = get_target_date(tokens[1] if len(tokens) == 2 else None)
            except InvalidDateError:
                print("ERROR: The date argument must be a valid date formatted as YYYY-MM-DD")
                continue

            try:
                answers = load_answers(ANSWERS_FILE)
            except LoadAnswersError:
                print(f"ERROR: Unable to load the answers file {ANSWERS_FILE}")
                continue

            try:
                valid = load_valid(VALID_FILE)
            except LoadValidError:
                print(f"ERROR: Unable to load the valid words file {VALID_FILE}")
                continue

            valid = valid + [v for v in answers.values()]

            try:
                answer = get_answer(answers, target_date)
            except DateRangeError:
                date_range = get_answer_date_range(answers)
                print(f"ERROR: Wordy isn't available for {target_date}.")
                print(f"The available date range is {date_range[0]} through {date_range[1]}.")
                continue

            try:
                history_file = get_history_file()
                history = load_history(history_file)
            except LoadHistoryError:
                print(f"ERROR: Unable to load the history file {history_file}")
                continue

            if does_history_include_date(history, target_date):
                print(f"WARNING: You've already played wordy for {target_date}.")
                response = input("Really play again? [y/N] ").lower()
                if response != "y":
                    print(f"OK: Not replaying wordy for {target_date}.")
                    continue

            print(f"OK: Playing wordy for {target_date}.")
            status = play(answer, valid)

            update_history(history, target_date, status)

            try:
                save_history(history, history_file)
            except SaveHistoryError:
                print(f"ERROR: Unable to save the history file {history_file}")
                continue

            print_stats(history)

        elif tokens[0] == "q" or tokens[0] == "quit":
            sys.exit(0)

        elif tokens[0] == "s" or tokens[0] == "stats":
            try:
                history_file = get_history_file()
                history = load_history(history_file)
            except LoadHistoryError:
                print(f"ERROR: Unable to load the history file {history_file}")
                continue

            print_stats(history)

        else:
            print("Please enter a valid command. Type h for help.")


class InvalidDateError(Exception):
    pass


def get_target_date(date_arg=None):
    if date_arg == None:
        return date.today()
    try:
        return date.fromisoformat(date_arg)
    except ValueError:
        raise InvalidDateError(date_arg)


class LoadAnswersError(Exception):
    pass


def load_answers(answers_file):
    try:
        with open(answers_file) as fh:
            return json.load(fh)
    except:
        raise LoadAnswersError()


class DateRangeError(Exception):
    pass


def get_answer_date_range(answers):
    all_dates = [date.fromisoformat(k) for k in answers.keys()]
    min_date = min(all_dates)
    max_date = max(all_dates)
    return (min_date, max_date)


def get_answer(answers, target_date):
    try:
        return answers[target_date.isoformat()]
    except KeyError:
        raise DateRangeError()


class LoadHistoryError(Exception):
    pass


def get_history_file():
    base_data_dir = Path.home() / ".local" / "share"
    if "XDG_DATA_HOME" in os.environ:
        base_data_dir = Path(os.environ["XDG_DATA_HOME"])
    data_dir = base_data_dir / "wordy"
    data_dir.mkdir(parents=True, exist_ok=True)
    return data_dir / "history.json"


def load_history(history_file):
    if history_file.exists():
        with open(history_file) as fh:
            return json.load(fh)
    else:
        return {}


def does_history_include_date(history, target_date):
    return target_date.isoformat() in history


class LoadValidError(Exception):
    pass


def load_valid(valid_file):
    try:
        return Path(valid_file).read_text().split("\n")
    except:
        raise LoadValidError()


def update_history(history, target_date, status):
    history[target_date.isoformat()] = status


class SaveHistoryError(Exception):
    pass


def save_history(history, history_file):
    try:
        with open(history_file, mode="w") as fh:
            json.dump(history, fh, indent=2)
    except:
        raise SaveHistoryError()


def print_stats(history):
    wins = 0
    losses = 0
    distrib = [0] * (MAX_GUESSES + 1)
    for game in history.values():
        if game["win"]:
            wins += 1
            distrib[len(game["guesses"]) - 1] += 1
        else:
            losses += 1
            distrib[MAX_GUESSES] += 1

    cum_distrib = [sum(distrib[:i + 1]) for i in range(len(distrib))]

    num_games = wins + losses

    if num_games > 0:
        win_pct_clause = f"{100 * wins / num_games:5.1f}%"
    else:
        win_pct_clause = "0%"
    print(f"Winning pct: {wins:,d}/{num_games:,d} {win_pct_clause}")
    print()

    print("                      Cumul   Cumul")
    print("Tries  Games     Pct  Games     Pct")
    print("-----  -----  ------  -----  ------")
    for i in range(len(distrib)):
        if i == MAX_GUESSES:
            tries = " loss"
        else:
            tries = f"{i + 1:5d}"

        freq = f"{distrib[i]:5,d}"
        cum_freq = f"{cum_distrib[i]:5,d}"

        if num_games > 0:
            pct = f"{100 * distrib[i] / num_games:5.1f}%"
            cum_pct = f"{100 * cum_distrib[i] / num_games:5.1f}%"
        else:
            pct = "0%"
            cum_pct = "0%"

        print(f"{tries}  {freq}  {pct}  {cum_freq}  {cum_pct}")

    print()


@unique
class Result(Enum):
    UNCHECKED = auto()
    CORRECT = auto()
    WRONG_PLACE = auto()
    ABSENT = auto()


def play(answer, valid):
    status = {
        "win": False,
        "answer": answer,
        "guesses": [],
    }

    while len(status["guesses"]) < MAX_GUESSES:
        guess = input(f"{len(status['guesses']) + 1}/{MAX_GUESSES}> ")
        guess = guess.strip().lower()

        if guess == answer:
            status["guesses"].append(guess)
            status["win"] = True
            del status["answer"]
            print("Correct!")
            return status

        if guess not in valid:
            print("Not a word.")
            continue

        status["guesses"].append(guess)
        comparison = compare(guess, answer)
        output = format_comparison(comparison, guess)
        print("     " + output)

    print(f"The correct answer is {answer}.")

    return status


def format_comparison(comparison, guess):
    output = []
    for i in range(len(comparison)):
        if comparison[i] == Result.ABSENT:
            output.append("-")
        elif comparison[i] == Result.WRONG_PLACE:
            output.append("*")
        elif comparison[i] == Result.CORRECT:
            output.append(guess[i])
    return "".join(output)


def compare(guess, answer):
    results = [Result.UNCHECKED] * len(guess)
    found = [False] * len(guess)

    for i in range(len(guess)):
        if guess[i] == answer[i]:
            results[i] = Result.CORRECT
            found[i] = True

    for i in range(len(guess)):
        if results[i] != Result.UNCHECKED:
            continue
        for j in range(len(answer)):
            if guess[i] == answer[j] and not found[j]:
                results[i] = Result.WRONG_PLACE
                found[j] = True
                break

    for i in range(len(guess)):
        if results[i] == Result.UNCHECKED:
            results[i] = Result.ABSENT

    return results


if __name__ == "__main__":
    main()
