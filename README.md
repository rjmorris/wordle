# Wordy

Wordy is a command-line implementation of the online game [Wordle](https://www.nytimes.com/games/wordle). A few notable points about this implementation:

- The word lists (both the daily answers and the valid words) match the ones provided by the game's original author, Josh Wardle. No effort has been made to track changes to the word lists after the game's sale to the New York Times.
- You can play a game from a past (or future) date.
- You can replay a game from a date you've already played.

## Installation

Wordy is a Python package, so you can use your favorite Python package installation method. The recommended method is [pipx](https://github.com/pypa/pipx):

```
$ pipx install git+https://gitlab.com/rjmorris/wordy.git@vX.Y.Z
```

Be sure to replace `X.Y.Z` with the version you want to install.

## Usage

Start Wordy with:

```
$ wordy
```

or:

```
$ python -m wordy
```

This will give you a prompt where you can enter commands. Play (or replay) today's Wordy with the `play` (or simply `p`) command:

```
> play
```

Play (or replay) Wordy for a specific date with:

```
> play 2021-06-19
```

Other commands are:

- `stats` (or `s`): Print your statistics.
- `help` (or `h`): View a list of available commands.
- `quit (or `q`): Quit.

You can also pass a command when starting Wordy. Wordy will run that command before giving you a prompt. For example, if you want it to immediately start playing today's Wordy, you could start it as:

```
$ wordy play
OK: Playing wordy for YYYY-MM-DD.
1/6>
```

Or if you want it to print the stats on startup:

```
$ wordy stats
Winning pct: 65/65 100.00%
Number of guesses in wins:
  1:   0   0.00%
  2:   3   4.62%
  3:  17  26.15%
  4:  32  49.23%
  5:  10  15.38%
  6:   3   4.62%
>
```

## Gameplay

Enter your 5-letter guesses at the prompt. Feedback will be provided showing:

- Letters from your guess that don't appear in the answer. These appear in the feedback as `-`.
- Letters from your guess that appear in the answer but in a different position than your guess. These appear in the feedback as `*`.
- Letters from your guess that appear in the answer at the same position as your guess. These appear in the feedback as the letter itself.

For example, suppose you guess `tells` and the answer is `table`. Then the feedback will be `t*-l-`, meaning your `t` and second `l` are in the right positions, your `e` is in the wrong position, and your `s` and first `l` are wrong.

Note that in this example your first `l` is given the feedback `-`. You might have expected the feedback to be `*`, because an `l` does appear in a different position in the answer. However, the `l` in that different position was already accounted for by the feedback on your second `l`. Feedback of `t**l-` would have implied that the answer had two `l`s. For example, if you guess `tells` and the answer is `troll`, then the feedback would be `t-*l-`.

## History file

A file containing the history of all games you've played will be stored in `$XDG_DATA_HOME/wordy/history.json`, defaulting to `~/.local/share/wordy/history.json` if `$XDG_DATA_HOME` is unset. The history file records whether you won or lost, and it records your guesses.

If you replay Wordy for a given date, your new results will overwrite the old results in the history file.

## Development

The package doesn't currently import any third-party dependencies, so a package manager isn't required for managing dependencies. However, it does use some packaging concepts like resources and entrypoint scripts, so you'll need to install it to run it while developing:

1. Activate a virtual environment.
1. Install with `pip install -e .`.
1. Run with `python -m wordy`. (You might be able to run it as simply `wordy`, but if wordy is installed elsewhere on your path -- for example if you installed the stable version using pipx -- then you might run that one instead.)

The package isn't published to PyPI. Releases are created via tags published to GitLab:

1. Update the version number in `pyproject.toml`.
1. Commit that change with the message "Bump version to 1.2.3".
1. Tag that commit with `git tag -a v1.2.3` with a message formatted as:

    ```
    Version 1.2.3

    Changes since 1.2.2:

    - first change desription
    - second change description
    ```

1. Push the commit and tag with `git push --follow-tags`.
